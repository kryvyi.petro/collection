Courses/Guides
- [Generative AI for Marketing using Google Cloud](https://github.com/GoogleCloudPlatform/genai-for-marketing)
- [Vertex AI Samples](https://github.com/GoogleCloudPlatform/vertex-ai-samples)
- [MLOps with Vertex AI](https://github.com/GoogleCloudPlatform/mlops-with-vertex-ai)
- Introduction to Kubeflow - [Training + Certification](https://www.arrikto.com/kubeflow-pipelines-fundamentals-training-certification-registration)

Topics
- [MLOps with GCP Vertex AI](https://medium.com/@nesilbor38/mlops-with-gcp-vertex-ai-275b038c8e38)
- [Tuning an LLM in Vertex AI](https://markryan-69718.medium.com/tuning-an-llm-in-vertex-ai-f6e340736461)
- [Train and deploy AI models with GitLab and Google Cloud's Vertex AI](https://about.gitlab.com/blog/2023/06/08/training-and-deploying-ai-models-with-gitlab-and-vertex-ai/)
- [Vertex AI Pipelines: End-to-end implementation of a custom pipeline, using Kubeflow Pipelines v2 custom components](https://medium.com/@fdesvoix/vertex-ai-pipelines-end-to-end-implementation-of-a-custom-pipeline-de021f0d8ab1)

Games
- [The Arcade](https://go.qwiklabs.com/arcade)

