Tools
- framework for managing and maintaining multi-language pre-commit hooks - [pre-commit](https://pre-commit.com/)

Courses/Guides
- [DevOps Guide](https://github.com/Tikam02/DevOps-Guide)
- [DevOps Exercises](https://github.com/bregman-arie/devops-exercises)
- [Awesome Terraform](https://github.com/shuaibiyy/awesome-terraform)
- [GitOps Fundamentals](https://learning.codefresh.io/)
- [GitOps For Reliable Kubernetes](https://academy.traefik.io/courses/gitops-for-reliable-kubernetes?utm_content=243636007&utm_medium=social&utm_source=twitter&hss_channel=tw-4890312130)
- [Top Terraform Tools to Know in 2024](https://www.env0.com/blog/top-terraform-tools-to-know-in-2024)
- [Awesome Cloud Native Trainings](https://github.com/joseadanof/awesome-cloudnative-trainings?tab=readme-ov-file)
- [DevSecOps library](https://github.com/sottlmarek/DevSecOps)

Games
- [DevOps Games](https://devops.games/)
- [DevOps Party Games](https://devopspartygames.com/)
- [Killercoda (place where you open your browser and get instant access to a real Linux or Kubernetes environment ready to use)](https://killercoda.com/)
- [SadServers is a SaaS where users can test their Linux and DevOps troubleshooting skills on real Linux servers in a "Capture the Flag" fashion](https://github.com/fduran/sadservers)