- [Top 75 Kubernetes Questions and Answers](https://medium.com/@bubu.tripathy/top-75-kubernetes-questions-and-answers-d677a0b87d79)
- [CKAD Exercises](https://github.com/dgkanatsios/CKAD-exercises)
- [THE KUBERNETES NETWORKING GUIDE](https://www.tkng.io/)
- A Curated List of Kubernetes Tools - [Kubetools](https://collabnix.github.io/kubetools/)

- Envoy Fundamentals - [Course + Certification](https://academy.tetrate.io/courses/envoy-fundamentals)
- Istio Fundamentals - [Course + Certification](https://academy.tetrate.io/courses/istio-fundamentals)
- Certified Istio Administrator by Tetrate - [Certification](https://academy.tetrate.io/courses/certified-istio-administrator)